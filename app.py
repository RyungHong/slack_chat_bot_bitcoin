
# -*- coding: utf-8 -*-
import re
import urllib.request
import requests
from bs4 import BeautifulSoup
import json
from flask import Flask, request, make_response
from slack import WebClient
from slackeventsapi import SlackEventAdapter

global user_name
user_name = ""
global user_location
user_location = ""

global zu_or_bit

SLACK_TOKEN = ""
SLACK_SIGNING_SECRET = ""

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


def set_info(name, location):
    global user_name
    global user_location
    user_name = name
    user_location = location


def get_chart(text):
    print("@@@@@@@@@@@@@@@@@@@@@@get_chart@@@@@@@@@@@@@@@@@@@@@@@@")

    ret_target = text
    target = text.replace(" ", "").upper()
    keywords = []
    for page in range(1, 7):
        req = requests.get('https://finance.naver.com/sise/sise_market_sum.nhn?&page=' + str(page))
        html = req.text
        soup = BeautifulSoup(html, 'html.parser')
        # 주식이름이 들어갈 예정 (ex 유한양행, 삼성전자, 삼성물산 ) 만약 삼성 전자 => 삼성전자, LG 전자 -> LG전자

        # 크롤링

        # print(soup.find("table", class_="type_5"))
        for tr in soup.find("table", class_="type_2").find_all("tr"):
            if len(tr.select("td:nth-child(2)")) == 1:
                # print(tr.select("td:nth-child(2)")[0].get_text().strip().replace(" ","").upper() )
                if tr.select("td:nth-child(2)")[0].get_text().strip().replace(" ",
                                                                              "").upper() == target:  # 주식 이름 tr.select("td:nth-child(2)")[0].get_text()
                    for idx in range(2, 13):
                        keywords.append(tr.select("td:nth-child(" + str(idx) + ")")[0].get_text().strip())

        # keywords[0]  종목
        # keywords[1]  현재가
        # keywords[3]  등락비율

        sign = keywords[3][0]
        print(keywords[3])
        ret1 = user_name + \
               " 고객님! CJ 대한통운입니다.\n\n" + \
               keywords[0] + " " + keywords[1].replace(",", "-") + \
               "를 오늘 배달할 예정입니다.\n 송장번호: 35215334262345234" + \
               "\n배달 장소: " + user_location + \
               " 825-3\n\n" + \
               user_location + " 대한통운 이령홍\n\n tel) 010-5367-1564\n\n" + \
               "고객님 부재시 " + \
               text + \
               sign + " " + keywords[3].replace('.', '-')[1:-1] + "는 " + \
               "근처 편의점에 맡기겠습니다.\n\n" + "자세한 서비스는 상세보기를 이용하여 주시기 바랍니다.\n\n아울러 상세보기를 클릭하시면\n우편물 배달 사전안내를 위한 개인정보 수집 동의가 가능합니다.\n" + \
               "항상 CJ 대한통운을 이용해주셔서 감사합니다.\n\n통신망 상태에 따라 데이터 요금이 발생할 수 있습니다.\n"

        ret2 = ret_target + "\n현재 시세: " + keywords[1] + "\n등락 비율: " + keywords[3]

    return ret1, ret2


def get_price(text):
    global user_name
    global user_location
    print("@@@@@@@@@@@@@@@@@@@@@@get_price@@@@@@@@@@@@@@@@@@@@@@@@")
    req = requests.get('https://www.bithumb.com/')
    html = req.text
    soup = BeautifulSoup(html, 'html.parser')

    ret_target = text
    target = "assetReal" + text  # target을 받는다. ex) btc, eth
    price = soup.find("table", id="tableAsset").find("tbody", class_="coin_list").find("strong",
                                                                                       id=target)  # 현재 시세를 크롤링으로 가져온다.
    price = price.getText()[:-1]  # '원'은 빼고 숫자만 저장

    target = "assetRealPrice" + text.upper()
    # print(text)
    result = soup.find("table", id="tableAsset").find("tbody", class_="coin_list").find("strong", id=target)
    sign = result.getText().split()[0][0]
    result = result.getText().split()[0][1:-1]  # 변동 가격을 출력
    # print(result)

    # text 종목
    # price 현재 가격
    # result 변동률

    ret1 = user_name + \
           " 고객님! 우체국 택배입니다.\n\n" + \
           text + " " + price.replace(',', '-') + \
           "를 오늘 배달할 예정입니다.\n 송장번호: 64813248763157486" + \
           "\n배달 장소: " + user_location + \
           " 835-15\n\n" + \
           user_location + " 우체국 이령홍\n\n tel) 010-5367-1564\n\n" + \
           "고객님 부재시 " + \
           text + \
           sign + " " + result.replace(',', '-').replace('.', '-') + "는 " + \
           "근처 편의점에 맡기겠습니다.\n\n" + "자세한 서비스는 상세보기를 이용하여 주시기 바랍니다.\n\n아울러 상세보기를 클릭하시면\n우편물 배달 사전안내를 위한 개인정보 수집 동의가 가능합니다.\n" + \
           "항상 우체국 택배를 이용해주셔서 감사합니다.\n\n통신망 상태에 따라 데이터 요금이 발생할 수 있습니다.\n"

    ret2 = ret_target + "\n현재 시세: " + price + "\n변동 가격: " + sign + result

    return (ret1, ret2)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    global user_name
    global user_location

    req = request.get_json()
    print("metion" + str(req))
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    if len(text.split()) > 1:
        user_name = text.split()[1]
        user_location = text.split()[2]
    attachments = [
        {
            "text": "서비스를 선택하세요.",
            "fallback": "You are unable to choose a game",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "callback_id": "chase",
            "actions": [
                {
                    "name": "check bit con",
                    "text": "사용법",
                    "type": "button",
                    "style": "primary",
                    "value": "0"
                },
                {
                    "name": "check bit con",
                    "text": "우체국",
                    "type": "button",
                    "value": "8"
                },
                {
                    "name": "check bit con",
                    "text": "CJ 대한통운",
                    "type": "button",
                    "value": "9"
                },
            ]
        }
    ]
    slack_web_client.chat_postMessage(
        channel=channel,
        text="택배입니다.",
        attachments=attachments
    )



@app.route("/slack/message", methods=['POST'])
def incoming_slack_message():
    global user_name
    global user_location
    global zu_or_bit
    target_list = ['BTC', 'ETH', 'XRP', 'BCH', 'LTC', 'EOS', 'BSV', 'TRX', 'ADA', 'XLM', 'XMR', 'DASH', 'LINK', 'ETC',
                   'XEM', 'ZEC', 'BTG', 'QTUM', 'VET', 'BAT', 'OMG', 'BTT', 'BCD', 'HC', 'NPXS', 'WAVES', 'REP', 'ZRX',
                   'ICX', 'IOST', 'ZIL', 'XVG', 'AE', 'GXC', 'THETA', 'WTC', 'STEEM', 'ELF', 'MCO', 'BZNT', 'ENJ',
                   'SNT', 'MIX', 'GNT', 'STRAT', 'VALOR', 'WAX', 'HDAC', 'ORBS', 'CON', 'CMT', 'LRC', 'LOOM', 'PPT',
                   'TRUE', 'KNC', 'POWR', 'ETZ', 'PIVX', 'ABT', 'POLY', 'CTXC', 'ITC', 'BHP', 'MTL', 'ANKR', 'ROM',
                   'MITH', 'PAY', 'LBA', 'DAC', 'HYC', 'APIS', 'GTO', 'OCN', 'RDN', 'ETHOS', 'AUTO', 'RNT', 'INS',
                   'AMO', 'TMTG', 'SALT', 'ARN', 'PST', 'DACC', 'WET', 'PLY']
    form_json = json.loads(request.form["payload"])
    print(form_json)

    if form_json["type"] == "dialog_submission":  # 배송조회 혹은 배송지 설정으로 값을 입력받고 난 후
        print(form_json['submission']['loc_origin'])
        target = form_json['submission']['loc_origin'].upper()  # 조회하려는 코인 종목

        if zu_or_bit == 8:
            if target not in target_list:
                slack_web_client.chat_postMessage(
                    channel=form_json['channel']['id'],
                    text="해당 제품은 반송처리 되었습니다.",
                )
            else:
                ret1, ret2 = get_price(target)  # 가격을 출력한다.
                slack_web_client.chat_postMessage(
                    channel=form_json['channel']['id'],
                    text=ret1,
                    attachments=[
                        {
                            "fallback": "You are unable to choose a game",
                            "attachment_type": "default",
                            "callback_id": "chase",
                            "actions": [
                                {
                                    "name": "detail",
                                    "text": "상세보기",
                                    "type": "button",
                                    "value": target,
                                }
                            ]
                        }
                    ]
                )
        else:
            ret1, ret2 = get_chart(target)  # 가격을 출력한다.

            slack_web_client.chat_postMessage(
                channel=form_json['channel']['id'],
                text=ret1,
                attachments=[
                    {
                        "fallback": "You are unable to choose a game",
                        "attachment_type": "default",
                        "callback_id": "chase",
                        "actions": [
                            {
                                "name": "detail",
                                "text": "상세보기",
                                "type": "button",
                                "value": target,
                            }
                        ]
                    }
                ]
            )
        return make_response("", 200)
    elif form_json["type"] == "interactive_message":  # 처음 화면에서 버튼을 눌렀을 때 들어오는 조건문
        if form_json["actions"][0]["value"] == str(0):  # 설명서 버튼을 눌렀을 때
            attachments = [
                {
                    "title": "설명서",
                    "color": "#bd0811",
                    "text": "*0. 이름, 주소 등록*\n" +
                            "\t@봇 이름 주소\n" +
                            "\t이름과 주소를 입력 받는다.\n\n" +
                            "\t이후 @봇 으로 이용 가능\n" +

                            "\t*우체국 선택시* => 비트 코인 시세 확인\n" +
                            "\t*CJ 대한통운 선택시* => 주식 시세 확인\n\n\n" +

                            "*비트 코인 설명*\n" +
                            "\t*1. 실시간 코인 시세 확인* \n" +
                            "\t\t배송조회 버튼 클릭\n" +
                            "\t\t이름, 주소 설정하지 않을 시 0번 실행 필요\n"
                            "\t\t버튼 클릭시 종목 입력을 받습니다. ex) BTC, ETH(대소문자 상관 없음)\n" +
                            "\t\t*암호화폐* 실시간 시세 정보를 택배 정보 제공 서비스처럼 속여서 출력\n\n" +
                            "\t\tex)OOO고객님 비트 택배입니다.\n" +
                            "\t\t'ETH 364-500'를 오늘 배달할 예정입니다. \n" +
                            "\t\t=> ETH: 코인 종목\n\t\t364-500: 실시간 시세(364,500원)('-'은 ','로 생각)\n\n" +
                            "\t\t송장번호: 15489613216 \n\n" +
                            "\t\t배달 장소: OOO시 OOO동\n" +
                            "\t\t고객님 부재시 'ETH (+ or -) 5-200'는 근처 편의점에 맡기겠습니다. \n" +
                            "\t\t=> + or -: 가격 변동, 5-200: 5,200원\n\n" +
                            "\t\t상세보기 버튼을 클릭하면 원래 시세 정보를 제공\n\n\n"

                            "\t*2. 매수 링크*\n" +
                            "\t\t반송 링크 버튼 클릭\n" +
                            "\t\t바로 코인을 매수할 수 있는 링크가 열리게 됩니다.\n\n\n" +

                            "*주식 설명*\n" +

                            "\t*실시간 주식 정보 출력*\n" +
                            "\t\t실제 택배 배송정보처럼 보이기 위해 사용될 정보를 입력받는다.\n\n" +

                            "\t\t실시간 *주식* 정보를 택배 정보 제공 서비스처럼 속여서 출력\n\n" +
                            "\t\tex)OOO고객님 CJ  대한통운입니다.\n" +
                            "\t\t'금호타이어 126-500'를 오늘 배달할 예정입니다. \n" +
                            "\t\t=> 금호타이어: 코인 종목\n\t: 실시간 시세(126,500원)('-'은 ','로 생각)\n\n" +
                            "\t\t고객님 부재시 'ETH (+ or -) 2-24'는 근처 편의점에 맡기겠습니다. \n" +
                            "\t\t=> + or -: 등락율(가격 변동 비율) , 2-24: 2.24%\n\n" +
                            "\t\t상세보기 버튼을 클릭하면 원래 시세 정보를 제공\n\n\n" +
                            "*그 외*\n" +
                            "\t해당 제품은 반송처리 되었습니다.\n" +
                            "\t=> 해당 종목이 존재하지 않음.\n\n\n",
                    "mrkdwn_in": ["text"]
                }
            ]
            slack_web_client.chat_postMessage(
                channel=form_json['channel']['id'],
                attachments=attachments
            )
        elif form_json["actions"][0]["value"] == str(1):  # 배송  조회 버튼을 눌렀을 때
            if user_name is "":  # 유저 네임이 아직 설정이 안되어 있으면 아래 메세지를 보여주고 챗봇 종료
                slack_web_client.chat_postMessage(
                    channel=form_json['channel']['id'],
                    text="배송지 설정을 먼저 해주세요"
                )
                return make_response("", 200)
            else:  # 유저네임이 설정되어 있으면 조회할 코인 종목을 입력받을 창을 띄운다.
                dialogs = {
                    "callback_id": "dialog",
                    "title": "제품명 입력",
                    "submit_label": "제출",
                    "notify_on_cancel": True,
                    "state": "product",
                    "elements": [
                        {
                            "type": "text",
                            "label": "제품명",
                            "name": "loc_origin"
                        },

                    ]
                }
            slack_web_client.dialog_open(trigger_id=form_json['trigger_id'], dialog=dialogs)
        elif form_json["actions"][0]["value"] == str(4):
            target = form_json["actions"][0]["name"]

            if zu_or_bit == 8:
                ret1, ret2 = get_price(target)
            else:
                ret1, ret2 = get_chart(target)
            slack_web_client.chat_postMessage(
                channel=form_json['channel']['id'],
                text=ret1,
                attachments=[
                    {
                        "fallback": "You are unable to choose a game",
                        "attachment_type": "default",
                        "callback_id": "chase",
                        "actions": [
                            {
                                "name": "back",
                                "text": "상세보기",
                                "type": "button",
                                "value": target,
                            }
                        ]
                    }
                ]
            )
        elif form_json["actions"][0]["value"] == str(8):  # 우체국, 비트코인
            zu_or_bit = 8
            attachments = [
                {
                    "text": "서비스를 선택하세요.",
                    "fallback": "비트코인",
                    "color": "#3AA3E3",
                    "attachment_type": "default",
                    "callback_id": "chase",
                    "actions": [
                        {
                            "name": "check bit con",
                            "text": "배송 조회",
                            "type": "button",
                            "value": "1"
                        },
                        {
                            "name": "매수페이지 링크",
                            "text": "반송 링크",
                            "type": "button",
                            'url': "https://www.bithumb.com/trade/order/BTC",
                            "value": "2"
                        }
                    ]
                }
            ]
            slack_web_client.chat_postMessage(
                channel=form_json['channel']['id'],
                text="우체국 택배입니다.",
                attachments=attachments
            )
        elif form_json["actions"][0]["value"] == str(9):  # CJ, 주식
            zu_or_bit = 9
            attachments = [
                {
                    "text": "서비스를 선택하세요.",
                    "fallback": "주식",
                    "color": "#3AA3E3",
                    "attachment_type": "default",
                    "callback_id": "chase",
                    "actions": [
                        {
                            "name": "check bit con",
                            "text": "배송 조회",
                            "type": "button",
                            "value": "1"
                        }
                    ]
                }
            ]
            slack_web_client.chat_postMessage(
                channel=form_json['channel']['id'],
                text="CJ 대한통운입니다.",
                attachments=attachments
            )
        else:  # 상세 보기
            target = form_json["actions"][0]["value"].upper()
            if zu_or_bit == 8:
                ret1, ret2 = get_price(target)
            else:
                ret1, ret2 = get_chart(target)
            slack_web_client.chat_postMessage(
                channel=form_json['channel']['id'],
                text=ret2,
                attachments=[
                    {
                        "fallback": "You are unable to choose a game",
                        "attachment_type": "default",
                        "callback_id": "chase",
                        "actions": [
                            {
                                "name": target,
                                "text": "돌아가기",
                                "type": "button",
                                "value": "4",
                            }
                        ]
                    }
                ]
            )

    else:
        print(form_json["actions"][0]['value'])

    #     # .. do something with the req ..
    return make_response("", 200)


@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=8080)


